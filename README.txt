﻿﻿README
Monte Carlo Tree Search for winning at Ultimate Tic Tac Toe
Teammates:

Kaile Baran
Harmen Kang

mcts_modified:

For mcts_modified we reduced the explore faction parameter. We also modified new_leaf such that it will look for available untried_actions that will result in the game ending in a win, rather than picking one randomly as in mcts_vanilla. This ensures mcts will always make a move that will win a small square if one is available, rather than randomly choosing a move within the square.
